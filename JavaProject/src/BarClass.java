
public class BarClass extends Alpha {
	public void foo(String args) {
		System.out.println("Bar class foo");
	}

	public void bar(String args) {
		System.out.println("bar class br metod");
	}
	
	public static void main(String args[]) {
		Alpha alpha = new BarClass();
		alpha.foo("test","it");
		alpha.bar("test");
		
		BarClass bar = (BarClass) alpha;
		bar.foo("test","test");
		bar.bar("test");
	}
	
}
